#!/usr/bin/env bash
set -euo pipefail

#export SCRIPT_DIR=$(dirname $(readlink -f "$0"))

docker-compose down
docker-compose up -d
docker-compose run postgresdb bash
#docker exec -it rca_db /bin/bash