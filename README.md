# About the Repo
This repository contains a copy of docker compose files and associated scripts that I have used in my master thesis to run a container containing the PostgreSQL server.

# High Level Architecture
The following diagram, written in PlantUML is a a representation of the components involved in the contact tracing software that I designed in my master thesis:
```plantuml

@startuml
!include https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml
!define DEVICONS https://raw.githubusercontent.com/tupadr3/plantuml-icon-font-sprites/master/devicons
!define FONTAWESOME https://raw.githubusercontent.com/tupadr3/plantuml-icon-font-sprites/master/font-awesome-5

!include DEVICONS/angular.puml
!include DEVICONS/dotnet.puml
!include DEVICONS/java.puml
!include DEVICONS/msql_server.puml
!include DEVICONS/postgresql.puml
!include FONTAWESOME/server.puml
!include FONTAWESOME/envelope.puml

skinparam classAttributeIconSize 0

System_Boundary(t1,"TracingCore") {
    Container(backend_api, "API Application", "Java, Springboot Framework", "Provides contact-tracing functionality via API", "java")
    ContainerDb(database, "Database", "PostgreSQL Database", "Stores beacon location information, and contact tracing results.", "postgresql")
}

Rel_Back_Neighbor(database, backend_api, "Reads from and writes to", "sync, JDBC")
@enduml

```
